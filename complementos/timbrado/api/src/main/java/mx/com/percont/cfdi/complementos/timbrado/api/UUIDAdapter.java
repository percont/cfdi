package mx.com.percont.cfdi.complementos.timbrado.api;

import java.util.UUID;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class UUIDAdapter extends XmlAdapter<String, UUID> {

  @Override
  public UUID unmarshal(String value) {
    return UUID.fromString(value);
  }

  @Override
  public String marshal(UUID uuid) {
    return uuid.toString();
  }
}
