package mx.gob.sat.timbrefiscaldigital.v10.tests;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import mx.gob.sat.timbrefiscaldigital.v10.ObjectFactory;
import mx.gob.sat.timbrefiscaldigital.v10.TimbreFiscalDigital;
import org.junit.Test;

public class UnmarshallingStampTests {

  private static final String EXPECTED_VERSION = "1.0";
  private static final String VALID_STAMP_FILE = "valid_stamp.xml";

  @Test
  public void unmarshallValidStampXMLTest() throws JAXBException {

    InputStream stream = UnmarshallingStampTests.class.getClassLoader()
        .getResourceAsStream(VALID_STAMP_FILE);

    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

    Unmarshaller unmarshaller = context.createUnmarshaller();
    Object unmarshalledFile = unmarshaller.unmarshal(stream);
    assertThat(unmarshalledFile, is(allOf(notNullValue(), instanceOf(TimbreFiscalDigital.class))));

    TimbreFiscalDigital timbre = (TimbreFiscalDigital) unmarshalledFile;

    assertThat(timbre, allOf(hasProperty("version", is(EXPECTED_VERSION)),
        hasProperty("UUID", is(notNullValue())),
        hasProperty("fechaTimbrado", is(notNullValue())),
        hasProperty("selloCFD", is(notNullValue())),
        hasProperty("noCertificadoSAT", is(notNullValue())),
        hasProperty("selloSAT", is(notNullValue()))
    ));

  }

}
