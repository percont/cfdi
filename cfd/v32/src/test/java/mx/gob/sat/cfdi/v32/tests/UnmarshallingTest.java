package mx.gob.sat.cfdi.v32.tests;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import mx.gob.sat.cfdi.v32.CFDIv32;
import mx.gob.sat.cfdi.v32.ObjectFactory;
import org.junit.Test;

public class UnmarshallingTest {

  private static final String EXPECTED_VERSION = "3.2";
  private static final String VALID_FILE_WITH_STAMP = "valido_con_timbrado.xml";

  @Test
  public void unmarshallTest() throws JAXBException {

    InputStream stream = UnmarshallingTest.class.getClassLoader()
        .getResourceAsStream(VALID_FILE_WITH_STAMP);

    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
    Unmarshaller unmarshaller = context.createUnmarshaller();

    Object unmarshalledFile = unmarshaller.unmarshal(stream);
    assertThat(unmarshalledFile, is(allOf(notNullValue(), instanceOf(CFDIv32.class))));

    CFDIv32 CFDIv32 = (CFDIv32) unmarshalledFile;
    assertThat(CFDIv32.getVersion(), is(EXPECTED_VERSION));

  }
}
