# SAT - Paquetes de Comprobante Fiscal Digital

Este es un esfuerzo para crear paquetes para manejar de manera fácil los elementos que forman el Comprobante Fiscal Digital del SAT utilizando tecnologías Java.

Creados originalmente para generar mi propia contabilidad, publico estos paquetes y código bajo licencia Apache 2.0.

## Elementos implementados ##

* Comprobante fiscal digital versión 3.2.
* Comprobante fiscal digital versión 3.3.
* Complemento de nómina versión 1.1.
* Complemento de nómina versión 1.2.
* Complemento de timbre fiscal digital 1.0.

## Por hacer ##

* Complemento de timbre fiscal digital 1.1.
* Cualquier otra cosa que se necesite en el inter.

